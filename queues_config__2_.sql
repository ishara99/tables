-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DELIMITER ;;

DROP PROCEDURE IF EXISTS `agent_call_history_repo`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `agent_call_history_repo`(IN _user_id INT(11), IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE incre_rowcount INT Default 0 ;
DECLARE rowcount INT Default 0 ;
DECLARE extension VARCHAR (50) ;
DECLARE i INT DEFAULT 0;
SET hours=0;
SET i=0;
-- SET incre_rowcount=0;

SET  rowcount := (SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` );

exten_loop: LOOP

IF incre_rowcount < 3 THEN

SET  extension := (SELECT `queues_config`.`extension` FROM `asterisk`.`queues_config` LIMIT incre_rowcount,1 );

simple_loop: LOOP

IF hours < 24 THEN


INSERT INTO tbl_agent_call_history_repo(
`userid`,
`username`,
`acd_calls`,
`avg_acw_times`,
`avg_acd_times`,
`hold_calls`,
`avg_hold_time`,
`avg_talk_time`,
`ob_calls`,
`ob_calls_tot`,
`trans_calls`
)																												
SELECT
`tbl_calls_evnt`.`agnt_userid` AS `userid`,
`user_master`.`username`,
(
SELECT
COUNT(tbl_calls_evnt.id) AS acd_calls
FROM
tbl_calls_evnt
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND tbl_calls_evnt.call_type = 'Inbound' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS acd_calls,
(
SELECT
SEC_TO_TIME(
ROUND(
SUM(tbl_calls_evnt.acw_sec_count) / acd_calls
)
) AS avg_acw_times
FROM
tbl_calls_evnt
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS avg_acw_times,
(
SELECT
SEC_TO_TIME(
ROUND(
SUM(
tbl_calls_evnt.answer_sec_count
) / acd_calls
)
) AS avg_acd_times
FROM
tbl_calls_evnt
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS avg_acd_times,
(
SELECT
COUNT(tbl_calls_hold_evnts.id) AS hold_calls
FROM
tbl_calls_evnt
INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS hold_calls,
(
SELECT
SEC_TO_TIME(
ROUND(
SUM(
tbl_calls_hold_evnts.hold_sec_count
) / hold_calls
)
) AS avg_hold_time
FROM
tbl_calls_evnt
INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS avg_hold_time,
(
SELECT
SEC_TO_TIME(
ROUND(
(
SUM(
tbl_calls_evnt.answer_sec_count
) - SUM(
tbl_calls_hold_evnts.hold_sec_count
)
) / acd_calls
)
) AS avg_talk_time
FROM
tbl_calls_evnt
INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS avg_talk_time,
(
SELECT
COUNT(tbl_calls_evnt.id) AS ob_calls
FROM
tbl_calls_evnt
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND tbl_calls_evnt.call_type = 'outbound' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS ob_calls,
(
SELECT
COUNT(tbl_calls_evnt.id) AS ob_calls
FROM
tbl_calls_evnt
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.call_type = 'outbound' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS ob_calls_tot,
(
SELECT
COUNT(tbl_calls_evnt.id) AS trans_calls
FROM
tbl_calls_evnt
WHERE
tbl_calls_evnt.agnt_userid = userid AND tbl_calls_evnt.status = 'ANSWER' AND tbl_calls_evnt.desc = 'BlindTransfer' AND(
tbl_calls_evnt.date BETWEEN _frm_date AND _to_date
)
) AS trans_calls
FROM
`tbl_calls_evnt`
INNER JOIN `user_master` ON `user_master`.`id` = `tbl_calls_evnt`.`agnt_userid`
WHERE
`tbl_calls_evnt`.`date` BETWEEN _frm_date AND _to_date AND `user_master`.`com_id` = 1
GROUP BY
`tbl_calls_evnt`.`agnt_userid`;


SET hours=hours+1;
ELSE 
LEAVE simple_loop;
END IF;
END LOOP simple_loop;


SET incre_rowcount=incre_rowcount+1;
ELSE 
LEAVE exten_loop;
END IF;
END LOOP exten_loop;


END;;

DROP PROCEDURE IF EXISTS `agent_detail_repo`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `agent_detail_repo`(IN _user_id INT(11), IN _com_id INT(11),IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
											INSERT INTO tbl_agent_detail_report (
														`userid`,
														`str_evntid`,
														`end_event`,
														`login_duration_dec`,
														`login_duration`,
														`break_time_sec`,
														`online_time_sec`,
														`break_time`,
														`answer_sec`,
														`answer_tot_calls`,
														`answer_ind_calls`,
														`answer_out_calls`,
														`acw_sec`,
														`acw_times`,
														`logout_datetime`,
														`login_datetime`,
														`user`,
														`username`,
														`active_time`,
														`idle_time`,
														`notready_time`
														)
											SELECT
											`tbl_agnt_evnt`.`agnt_userid` AS `userid`,
											`tbl_agnt_evnt`.`id` AS `str_evntids`,
											( SELECT tbl_agnt_evnt.agnt_event AS end_event FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS end_event,
											( SELECT ( tbl_agnt_evnt.evnt_min_count * 60 ) AS evnt_min_count FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS login_duration_dec,
											(
												SELECT
													SEC_TO_TIME(
													ROUND( login_duration_dec ))) AS login_duration,
											(
												SELECT
													( SUM( tbl_agnt_evnt.evnt_min_count ) * 60 ) AS evnt_min_count 
												FROM
													tbl_agnt_evnt 
												WHERE
													( tbl_agnt_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
													AND tbl_agnt_evnt.agnt_event = "Break End" 
													AND tbl_agnt_evnt.agnt_userid = userid 
												) AS break_time_sec,
											  (
													SELECT
														( SUM( tbl_agnt_evnt.evnt_min_count ) * 60 ) AS evnt_min_count 
													FROM
														tbl_agnt_evnt 
													WHERE
														( tbl_agnt_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
														AND tbl_agnt_evnt.agnt_event = "Offline" 
														AND tbl_agnt_evnt.agnt_userid = userid 
													) AS online_time_sec,
													(
													SELECT
														SEC_TO_TIME(
														ROUND( break_time_sec ))) AS break_time,
		                       (
														SELECT
															(
															SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec 
														FROM
															tbl_calls_evnt 
														WHERE
															tbl_calls_evnt.agnt_userid = userid 
															AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
															AND tbl_calls_evnt.STATUS = 'ANSWER' 
														) AS answer_sec,
														(
																	SELECT
																		(
																		count( tbl_calls_evnt.id )) AS answer_tot_calls 
																	FROM
																		tbl_calls_evnt 
																	WHERE
																		tbl_calls_evnt.agnt_userid = userid 
																		AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																		AND tbl_calls_evnt.STATUS = 'ANSWER' 
																	) AS answer_tot_calls,
																	(
																		SELECT
																			(
																			count( tbl_calls_evnt.id )) AS answer_ind_calls 
																		FROM
																			tbl_calls_evnt 
																		WHERE
																			tbl_calls_evnt.agnt_userid = userid 
																			AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																			AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			AND tbl_calls_evnt.call_type = 'Inbound' 
																		) AS answer_ind_calls,
																		(
																		SELECT
																			(
																			count( tbl_calls_evnt.id )) AS answer_out_calls 
																		FROM
																			tbl_calls_evnt 
																		WHERE
																			tbl_calls_evnt.agnt_userid = userid 
																			AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																			AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			AND tbl_calls_evnt.call_type = 'outbound' 
																		) AS answer_out_calls,
																	  (
																			SELECT
																				SEC_TO_TIME(
																				ROUND( answer_sec ))) AS answer_times,
																			(
																			SELECT
																				(
																				SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec 
																			FROM
																				tbl_calls_evnt 
																			WHERE
																				tbl_calls_evnt.agnt_userid = userid 
																				AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																				AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			) AS acw_sec,
																			( SELECT tbl_agnt_evnt.cre_datetime AS logout_datetime FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS logout_datetime,
	                    `tbl_agnt_evnt`.`cre_datetime` AS `login`,
											_user_id,
	                    `user_master`.`username`,
										(
	SELECT
		SEC_TO_TIME(
			ROUND(
			IFNULL( answer_sec, 0 )+ IFNULL( acw_sec, 0 )))) AS active_time,
		(
	SELECT
		SEC_TO_TIME(
			ROUND(
				login_duration_dec -(
				IFNULL( answer_sec, 0 )+ IFNULL( acw_sec, 0 ))))) AS idle_time,
			(
	SELECT
		SEC_TO_TIME(
			ROUND(
				login_duration_dec -(
				IFNULL( online_time_sec, 0 ))))) AS notready_time	
FROM
	`tbl_agnt_evnt`
	INNER JOIN `user_master` ON `user_master`.`id` = `tbl_agnt_evnt`.`agnt_userid` 
WHERE
	`tbl_agnt_evnt`.`cre_datetime` BETWEEN _frm_date 
	AND _to_date 
	AND `tbl_agnt_evnt`.`agnt_event` = 'Log IN Time' 
	AND `user_master`.`com_id` = _com_id 
	AND `tbl_agnt_evnt`.`id_of_prtone` IS NULL;
  
End;;

DROP PROCEDURE IF EXISTS `agent_detail_repo_agentwise`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `agent_detail_repo_agentwise`(IN _user_id INT(11), IN _com_id INT(11), IN _agent_id INT(11), IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
											INSERT INTO tbl_agent_detail_report (
														`userid`,
														`str_evntid`,
														`end_event`,
														`login_duration_dec`,
														`login_duration`,
														`break_time_sec`,
														`online_time_sec`,
														`break_time`,
														`answer_sec`,
														`answer_tot_calls`,
														`answer_ind_calls`,
														`answer_out_calls`,
														`acw_sec`,
														`acw_times`,
														`logout_datetime`,
														`login_datetime`,
														`user`,
														`username`,
														`active_time`,
														`idle_time`,
														`notready_time`
														)
											SELECT
											`tbl_agnt_evnt`.`agnt_userid` AS `userid`,
											`tbl_agnt_evnt`.`id` AS `str_evntids`,
											( SELECT tbl_agnt_evnt.agnt_event AS end_event FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS end_event,
											( SELECT ( tbl_agnt_evnt.evnt_min_count * 60 ) AS evnt_min_count FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS login_duration_dec,
											(
												SELECT
													SEC_TO_TIME(
													ROUND( login_duration_dec ))) AS login_duration,
											(
												SELECT
													( SUM( tbl_agnt_evnt.evnt_min_count ) * 60 ) AS evnt_min_count 
												FROM
													tbl_agnt_evnt 
												WHERE
													( tbl_agnt_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
													AND tbl_agnt_evnt.agnt_event = "Break End" 
													AND tbl_agnt_evnt.agnt_userid = userid 
												) AS break_time_sec,
											  (
													SELECT
														( SUM( tbl_agnt_evnt.evnt_min_count ) * 60 ) AS evnt_min_count 
													FROM
														tbl_agnt_evnt 
													WHERE
														( tbl_agnt_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
														AND tbl_agnt_evnt.agnt_event = "Offline" 
														AND tbl_agnt_evnt.agnt_userid = userid 
													) AS online_time_sec,
													(
													SELECT
														SEC_TO_TIME(
														ROUND( break_time_sec ))) AS break_time,
		                       (
														SELECT
															(
															SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec 
														FROM
															tbl_calls_evnt 
														WHERE
															tbl_calls_evnt.agnt_userid = userid 
															AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
															AND tbl_calls_evnt.STATUS = 'ANSWER' 
														) AS answer_sec,
														(
																	SELECT
																		(
																		count( tbl_calls_evnt.id )) AS answer_tot_calls 
																	FROM
																		tbl_calls_evnt 
																	WHERE
																		tbl_calls_evnt.agnt_userid = userid 
																		AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																		AND tbl_calls_evnt.STATUS = 'ANSWER' 
																	) AS answer_tot_calls,
																	(
																		SELECT
																			(
																			count( tbl_calls_evnt.id )) AS answer_ind_calls 
																		FROM
																			tbl_calls_evnt 
																		WHERE
																			tbl_calls_evnt.agnt_userid = userid 
																			AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																			AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			AND tbl_calls_evnt.call_type = 'Inbound' 
																		) AS answer_ind_calls,
																		(
																		SELECT
																			(
																			count( tbl_calls_evnt.id )) AS answer_out_calls 
																		FROM
																			tbl_calls_evnt 
																		WHERE
																			tbl_calls_evnt.agnt_userid = userid 
																			AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																			AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			AND tbl_calls_evnt.call_type = 'outbound' 
																		) AS answer_out_calls,
																	  (
																			SELECT
																				SEC_TO_TIME(
																				ROUND( answer_sec ))) AS answer_times,
																			(
																			SELECT
																				(
																				SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec 
																			FROM
																				tbl_calls_evnt 
																			WHERE
																				tbl_calls_evnt.agnt_userid = userid 
																				AND ( tbl_calls_evnt.cre_datetime BETWEEN _frm_date AND _to_date ) 
																				AND tbl_calls_evnt.STATUS = 'ANSWER' 
																			) AS acw_sec,
																			( SELECT tbl_agnt_evnt.cre_datetime AS logout_datetime FROM tbl_agnt_evnt WHERE tbl_agnt_evnt.id_of_prtone = str_evntids LIMIT 1 ) AS logout_datetime,
	                    `tbl_agnt_evnt`.`cre_datetime` AS `login`,
											_user_id,
	                    `user_master`.`username`, 
											(
	SELECT
		SEC_TO_TIME(
			ROUND(
			IFNULL( answer_sec, 0 )+ IFNULL( acw_sec, 0 )))) AS active_time,
		(
	SELECT
		SEC_TO_TIME(
			ROUND(
				login_duration_dec -(
				IFNULL( answer_sec, 0 )+ IFNULL( acw_sec, 0 ))))) AS idle_time,
			(
	SELECT
		SEC_TO_TIME(
			ROUND(
				login_duration_dec -(
				IFNULL( online_time_sec, 0 ))))) AS notready_time	
FROM
	`tbl_agnt_evnt`
	INNER JOIN `user_master` ON `user_master`.`id` = `tbl_agnt_evnt`.`agnt_userid` 
WHERE
	`tbl_agnt_evnt`.`cre_datetime` BETWEEN _frm_date 
	AND _to_date 
	AND `tbl_agnt_evnt`.`agnt_event` = 'Log IN Time' 
	AND `user_master`.`com_id` = _com_id
AND `tbl_agnt_evnt`.`agnt_userid` = _agent_id	
	AND `tbl_agnt_evnt`.`id_of_prtone` IS NULL;
  
End;;

DROP PROCEDURE IF EXISTS `prcAbandon`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcAbandon`(IN _callid VARCHAR(40))
BEGIN
  CALL prcDeleteQueue(_callid);
END;;

DROP PROCEDURE IF EXISTS `prcCallComplete`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCallComplete`(IN _agent int(6),
  IN _callid varchar(40),
  IN _dst_number int(9),
  IN _queue_id int(6))
BEGIN
  CALL prcDeleteQueue(_callId);
  CALL prcLiveSend(_agent,NULL,NULL);
END;;

DROP PROCEDURE IF EXISTS `prcCallConnect`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCallConnect`(IN _callid VARCHAR(40),
  IN _agent VARCHAR(20), 
  IN _data VARCHAR(100))
    MODIFIES SQL DATA
BEGIN

  UPDATE queue_log SET
    time = NOW(),
    agent = _agent,
    event = 'CONNECT'
    WHERE callid = _callid;

  UPDATE live_status SET status='CONNECTED' WHERE sip_id = _agent;

END;;

DROP PROCEDURE IF EXISTS `prcCreateCamp`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcCreateCamp`(IN _batch_id INT(11))
BEGIN

-- DELETE FROM `phonikip_db`.`autodial_batch` where `call_schedule`='0000-00-00 00:00:00';
-- INSERT IGNORE INTO `phonikip_db`.autodial_master (campaign_id, cus_number, cus_name, cus_nic, cus_type, cus_add, cus_cover_date, cus_deduction)

 -- SELECT batch_id, cus_number, cus_name, cus_nic, cus_type, cus_address, cus_cover_date, cus_deduction FROM `phonikip_db`.autodial_csv_data WHERE batch_id=_batch_id;
 -- INSERT IGNORE INTO `fairfirst`.tbl_customer_master (tbl_customer_master.batchID,  tbl_customer_master.iMobileNumber)
 SELECT batch_id,contact_no FROM autodial_csv_data WHERE batch_id=_batch_id;

SET @n=(SELECT MAX(id) FROM autodial_csv_data WHERE batch_id=_batch_id);
SET @i=(SELECT MIN(id) FROM autodial_csv_data WHERE batch_id=_batch_id);
SET @batch_id=_batch_id;
 

PREPARE stmt FROM
"SELECT contact_no
FROM autodial_csv_data
WHERE batch_id=@batch_id AND id=?
INTO @cusNumber";
 
WHILE @i<=@n DO
EXECUTE stmt USING @i;
 
INSERT IGNORE INTO autodial_batch (campaign_id, cus_number,cus_language,batch_id,dnt_cl_dup_batch,facility_no,user_id)
SELECT campaign_id, cus_number,cus_language,batch_id,dnt_cl_dup_batch,facility_no,user_id
FROM (
SELECT campign_id AS campaign_id,
contact_no AS cus_number,
batch_id AS batch_id,
lang AS cus_language,
dnt_cl_dup_batch AS dnt_cl_dup_batch,
facility_no AS facility_no,
user_id AS user_id
FROM autodial_csv_data
WHERE batch_id=_batch_id AND id=@i AND contact_no!=0
)t
WHERE NOT EXISTS(SELECT 1 FROM autodial_remark t2
WHERE (t2.call_status = 'ANSWERED' OR t2.attempt>3) and t2.cus_number=@cusNumber);

SET @i=@i+1;

END WHILE;
DELETE FROM `autodial_csv_data` WHERE `batch_id`=_batch_id;
END;;

DROP PROCEDURE IF EXISTS `prcDeleteQueue`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcDeleteQueue`(_callid VARCHAR(40))
    MODIFIES SQL DATA
BEGIN
    DELETE FROM queue_log WHERE callid=_callid;
END;;

DROP PROCEDURE IF EXISTS `prcEnterQueue`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcEnterQueue`(IN _created DATETIME, 
  IN _callid VARCHAR(40),
  IN _queuename VARCHAR(20),
  IN _agent VARCHAR(20),
  IN _event VARCHAR(20),
  IN _data VARCHAR(100))
    MODIFIES SQL DATA
BEGIN
  INSERT INTO queue_log (time,callid,queuename,agent,event,data) 
  values (_created,_callid,_queuename,_agent,_event,_data);
END;;

DROP PROCEDURE IF EXISTS `prcLiveSend`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcLiveSend`(IN _sip_id int(6),
  IN _cli  INT(14),
  IN _queuename varchar(45))
BEGIN

  UPDATE live_status
  SET cli = _cli,
      queuename = _queuename
  WHERE sip_id = _sip_id;

END;;

DROP PROCEDURE IF EXISTS `prcRingNoAnswer`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcRingNoAnswer`(IN _callid VARCHAR(40))
BEGIN

END;;

DROP PROCEDURE IF EXISTS `queue_hourly`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `queue_hourly`(IN _user_id INT(11), IN _com_id INT(11), IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE incre_rowcount INT Default 0 ;
DECLARE rowcount INT Default 0 ;
DECLARE extension VARCHAR (50) ;
DECLARE i INT DEFAULT 0;
SET hours=0;
SET i=0;
-- SET incre_rowcount=0;

		SET  rowcount := (SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` );

		exten_loop: LOOP
								
		IF incre_rowcount < 3 THEN
		
		SET  extension := (SELECT `queues_config`.`extension` FROM `asterisk`.`queues_config` LIMIT incre_rowcount,1 );
		
							simple_loop: LOOP
								
							 IF hours < 24 THEN
							 
							 
									INSERT INTO tbl_queue_hrly_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														--  `avg_answer_times`,
														`answer_acwtimes`,
														-- `avg_answer_acwtimes`,
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														-- `service_level`,
														-- `tot_abn_rate`,
														-- `tot_abn_rate_sl`,
														-- `tot_abn_rate_outofsl`,
														`hold_calls`,
														`hold_time`,
														-- `avg_hold_time`,
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														-- `asa`
														)
										SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS offerd_calls,
										      
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND (
												    HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											      AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls_sl,
													-- ( SELECT SUM( answer_call_sl /( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS service_level,
													-- ( SELECT SUM(( tot_abn_calls )/( offerd_calls )* 100 ) ) AS tot_abn_rate,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS tot_abn_rate_sl,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls )* 100 ) ) AS tot_abn_rate_outofsl,
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS transout_calls,
													`tbl_calls_evnt`.`date` AS `date_or_hour`,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
													`queues_config`.`com_id` = _com_id 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)
												GROUP BY
													`tbl_calls_evnt`.`agnt_queueid` LIMIT i,1;
											
											

								
								
								
							 SET hours=hours+1;
							 ELSE 
							 LEAVE simple_loop;
							 END IF;
				 END LOOP simple_loop;
				
				
		SET incre_rowcount=incre_rowcount+1;
		ELSE 
			LEAVE exten_loop;
		END IF;
		END LOOP exten_loop;


END;;

DROP PROCEDURE IF EXISTS `queue_repo_day`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `queue_repo_day`(IN _user_id INT(11), IN _com_id INT(11), IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` INTO n;
SET i=0;
WHILE i<n DO 
											INSERT INTO tbl_queue_day_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														--  `avg_answer_times`,
														`answer_acwtimes`,
														-- `avg_answer_acwtimes`,
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														-- `service_level`,
														-- `tot_abn_rate`,
														-- `tot_abn_rate_sl`,
														-- `tot_abn_rate_outofsl`,
														`hold_calls`,
														`hold_time`,
														-- `avg_hold_time`,
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														-- `asa`
														)
											SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS offerd_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS tot_abn_calls_sl,
													-- ( SELECT SUM( answer_call_sl /( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS service_level,
													-- ( SELECT SUM(( tot_abn_calls )/( offerd_calls )* 100 ) ) AS tot_abn_rate,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS tot_abn_rate_sl,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls )* 100 ) ) AS tot_abn_rate_outofsl,
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS transout_calls,
													`tbl_calls_evnt`.`date` AS `date_or_hour`,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND ( tbl_calls_evnt.date = cur_date ) 
													GROUP BY
														tbl_calls_evnt.date 
													) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
													`queues_config`.`com_id` = _com_id 
													AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
												GROUP BY
													`tbl_calls_evnt`.`date`,
													`tbl_calls_evnt`.`agnt_queueid` LIMIT i,1;
  SET i = i + 1;
END WHILE;
End;;

DROP PROCEDURE IF EXISTS `queue_repo_hourly`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `queue_repo_hourly`(IN _user_id INT(11), IN _com_id INT(11), IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
DECLARE hours INT Default 0 ;
DECLARE i INT DEFAULT 0;
DECLARE n INT DEFAULT 0;
SET hours=0;
SET i=0;

SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` INTO n;
WHILE i<n DO 
simple_loop: LOOP
								
							 IF hours < 24 THEN
							 
							 
									INSERT INTO tbl_queue_hrly_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														--  `avg_answer_times`,
														`answer_acwtimes`,
														-- `avg_answer_acwtimes`,
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														-- `service_level`,
														-- `tot_abn_rate`,
														-- `tot_abn_rate_sl`,
														-- `tot_abn_rate_outofsl`,
														`hold_calls`,
														`hold_time`,
														-- `avg_hold_time`,
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														-- `asa`
														)
										SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS offerd_calls,
										      
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND (
												    HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											      AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS tot_abn_calls_sl,
													-- ( SELECT SUM( answer_call_sl /( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS service_level,
													-- ( SELECT SUM(( tot_abn_calls )/( offerd_calls )* 100 ) ) AS tot_abn_rate,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS tot_abn_rate_sl,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls )* 100 ) ) AS tot_abn_rate_outofsl,
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS transout_calls,
													hours,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
													AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND (`tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date))) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND (
												HOUR ( tbl_calls_evnt.cre_datetime ) = hours 
											AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date)) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
												`queues_config`.`com_id` = _com_id 
													AND
										(
											`tbl_calls_evnt`.`date` between _frm_date and _to_date AND 
											HOUR ( tbl_calls_evnt.cre_datetime ) = hours AND tbl_calls_evnt.agnt_queueid = extension 
										) GROUP BY tbl_calls_evnt.agnt_queueid;
										IF ROW_COUNT() = 0 THEN
							
										INSERT INTO tbl_queue_hrly_report (
															`userid`,
															`extension`,
															`descr`,
															`date_or_hour`
														)
														SELECT
															_user_id,
														`queues_config`.`extension`,
														`queues_config`.`descr`,
														hours
											
														FROM
															`asterisk`.`queues_config` where  `queues_config`.`extension` = extension
															AND `asterisk`.`queues_config`.`com_id` = _com_id;
								END IF; 		
										

								
								
								
							 SET hours=hours+1;
							 ELSE 
							 LEAVE simple_loop;
							 END IF;
				 END LOOP simple_loop;
				
				
		 SET i = i + 1;
END WHILE;
End;;

DROP PROCEDURE IF EXISTS `queue_repo_sum`;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `queue_repo_sum`(IN _user_id INT(11), IN _com_id INT(11), IN _to_date VARCHAR(50), IN _frm_date VARCHAR (50))
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
SELECT COUNT(`queues_config`.`extension`) FROM `asterisk`.`queues_config` INTO n;
SET i=0;
WHILE i<n DO 
											INSERT INTO tbl_queue_summary_report (
														`userid`,
														`queueid`,
														`cur_date`,
														`cur_datetime`,
														`hour`,
														`extension`,
														`descr`,
														`offerd_calls`,
														`answer_calls`,
														`ans_calls`,
														`answer_times`,
														--  `avg_answer_times`,
														`answer_acwtimes`,
														-- `avg_answer_acwtimes`,
														`tot_ht`,
														`answer_call_sl`,
														`tot_abn_calls`,
														`tot_abn_calls_sl`,
														-- `service_level`,
														-- `tot_abn_rate`,
														-- `tot_abn_rate_sl`,
														-- `tot_abn_rate_outofsl`,
														`hold_calls`,
														`hold_time`,
														-- `avg_hold_time`,
														`transout_calls`,
														`date_or_hour`,
														`ring_sec_count`,
														`answer_sec_count`,
														`acw_sec_count`,
														`hold_sec_count`,
														`ring_sec_count_new`												
														-- `asa`
														)
											SELECT _user_id,
													`tbl_calls_evnt`.`agnt_queueid` AS `queueid`,
													`tbl_calls_evnt`.`date` AS cur_date,
													`tbl_calls_evnt`.`cre_datetime` AS `cur_datetime`,
													HOUR ( tbl_calls_evnt.cre_datetime ) AS HOUR,
													`queues_config`.`extension`,
													`queues_config`.`descr`,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS offerd_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS offerd_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ENTERQUEUE' 
														AND ( tbl_calls_evnt.DESC = 'COMPLETECALLER' OR tbl_calls_evnt.DESC = 'COMPLETEAGENT' ) 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS answer_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS ans_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS ans_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.answer_sec_count ))) AS answer_times 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS answer_times,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_evnt.acw_sec_count ))) AS answer_acwtimes 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS answer_acwtimes,
													
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
																SUM(
																tbl_calls_evnt.answer_sec_count + IFNULL( tbl_calls_evnt.acw_sec_count, 0 )))) AS tot_ht 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS tot_ht,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS answer_call_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND tbl_calls_evnt.ring_sec_count <= 7 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS answer_call_sl,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS tot_abn_calls,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS tot_abn_calls_sl 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'ABANDON' 
														AND tbl_calls_evnt.ring_sec_count <= 5 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS tot_abn_calls_sl,
													-- ( SELECT SUM( answer_call_sl /( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS service_level,
													-- ( SELECT SUM(( tot_abn_calls )/( offerd_calls )* 100 ) ) AS tot_abn_rate,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls - tot_abn_calls_sl )* 100 ) ) AS tot_abn_rate_sl,
													-- ( SELECT SUM(( tot_abn_calls - tot_abn_calls_sl )/( offerd_calls )* 100 ) ) AS tot_abn_rate_outofsl,
													(
													SELECT
														COUNT( tbl_calls_hold_evnts.id ) AS hold_calls 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS hold_calls,
													(
													SELECT
														SEC_TO_TIME(
															ROUND(
															SUM( tbl_calls_hold_evnts.hold_sec_count ))) AS hold_time 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS hold_time,
													(
													SELECT
														COUNT( tbl_calls_evnt.id ) AS transout_calls 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.DESC = 'BlindTransfer' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS transout_calls,
													`tbl_calls_evnt`.`date` AS `date_or_hour`,
													(SELECT
														SEC_TO_TIME(
														ROUND( SUM( tbl_calls_evnt.ring_sec_count ) )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS ring_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.answer_sec_count )) AS answer_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS answer_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.acw_sec_count )) AS acw_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date
													) AS acw_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_hold_evnts.hold_sec_count )) AS hold_sec_count 
													FROM
														tbl_calls_evnt
														INNER JOIN tbl_calls_hold_evnts ON tbl_calls_evnt.linkedid = tbl_calls_hold_evnts.linkedid 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND tbl_calls_evnt.STATUS = 'ANSWER' 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
													) AS hold_sec_count,
													(
													SELECT
														ROUND( SUM( tbl_calls_evnt.ring_sec_count )) AS ring_sec_count 
													FROM
														tbl_calls_evnt 
													WHERE
														tbl_calls_evnt.agnt_queueid = extension 
														AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date  
													) AS ring_sec_count
													FROM
													`tbl_calls_evnt`
													INNER JOIN `asterisk`.`queues_config` ON `queues_config`.`extension` = `tbl_calls_evnt`.`agnt_queueid` 
												WHERE
													`queues_config`.`com_id` = _com_id 
													AND `tbl_calls_evnt`.`cre_datetime` BETWEEN _frm_date 
													AND _to_date 
												GROUP BY
													`tbl_calls_evnt`.`agnt_queueid` LIMIT i,1;
  SET i = i + 1;
END WHILE;
End;;

DELIMITER ;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `queues_config`;
CREATE TABLE `queues_config` (
  `extension` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `descr` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `grppre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alertinfo` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ringing` tinyint(1) NOT NULL DEFAULT 0,
  `maxwait` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ivr_id` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `dest` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cwignore` tinyint(1) NOT NULL DEFAULT 0,
  `queuewait` tinyint(1) DEFAULT 0,
  `use_queue_context` tinyint(1) DEFAULT 0,
  `togglehint` tinyint(1) DEFAULT 0,
  `qnoanswer` tinyint(1) DEFAULT 0,
  `callconfirm` tinyint(1) DEFAULT 0,
  `callconfirm_id` int(11) DEFAULT NULL,
  `qregex` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agentannounce_id` int(11) DEFAULT NULL,
  `joinannounce_id` int(11) DEFAULT NULL,
  `monitor_type` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monitor_heard` int(11) DEFAULT NULL,
  `monitor_spoken` int(11) DEFAULT NULL,
  `callback_id` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `com_id` int(2) NOT NULL DEFAULT 1,
  PRIMARY KEY (`extension`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;


-- 2021-02-10 06:09:00
